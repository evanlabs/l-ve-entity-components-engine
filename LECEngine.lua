-- We use the LECEngine table as a namespace for all the LUA Entity Components Engine's public classes.

local OOImplementation = require "LECEngine/Core/ObjectOrientedImplementation"
LECEngine = OOImplementation.newClass()

LECEngine.Core = {}
LECEngine.Core.OOImplementation = OOImplementation

LECEngine.Utils = {}
LECEngine.Utils.Vector = require "LECEngine/Utils/Vector2"

local Component = require "LECEngine/Core/Component"
LECEngine.Core.Component = Component
local LogicSystem = require "LECEngine/Core/LogicSystem"
LECEngine.LogicComponent = require "LECEngine/Components/Abstract/LogicComponent"
local GraphicSystem = require "LECEngine/Core/GraphicSystem"
LECEngine.GraphicComponent = require "LECEngine/Components/Abstract/GraphicComponent"
LECEngine.MixedComponent = require "LECEngine/Components/Abstract/MixedComponent"

LECEngine.Components = {}
LECEngine.Components.Transform = require "LECEngine/Components/Transform"
LECEngine.Components.SpriteRenderer = require "LECEngine/Components/SpriteRenderer"

LECEngine.Entity = require "LECEngine/Core/Entity"

function LECEngine.new()
	local engine = LECEngine:create()
	LECEngine._instance = engine
	engine._logicSystem = LogicSystem.new()
	engine._graphicSystem = GraphicSystem.new()
	return engine
end

function LECEngine.get()
	return LECEngine._instance
end

function LECEngine:update(p_dt)
	self._logicSystem:update(p_dt)
end

function LECEngine:draw()
	self._graphicSystem:draw()
end

--isInstanceOf can be heavy on deep heritence hierarchy
function LECEngine:addComponent(p_component)
	local added = false
	if p_component:isInstanceOf(LECEngine.LogicComponent)
	  or p_component:isInstanceOf(LECEngine.MixedComponent) then
		self._logicSystem:addComponent(p_component)
		added = true
	end
	if p_component:isInstanceOf(LECEngine.GraphicComponent)
	  or p_component:isInstanceOf(LECEngine.MixedComponent) then
		self._graphicSystem:addComponent(p_component)
		added = true
	end
	if not added then
		error ("Component #" .. p_component:getId() .. " doesn't match any known component type")
	end
end

function LECEngine:removeComponent(p_component)
	local deleted = false
	if p_component:isInstanceOf(LECEngine.LogicComponent)
	  or p_component:isInstanceOf(LECEngine.MixedComponent) then
		self._logicSystem:removeComponent(p_component)
		deleted = true
	end
	if p_component:isInstanceOf(LECEngine.GraphicComponent)
	  or p_component:isInstanceOf(LECEngine.MixedComponent) then
	  	self._graphicSystem:removeComponent(p_component)
	  	deleted = true
	end
	if not deleted then
		error "No component type match for this class"
	end
end

-- We instantiate the engine singleton
LECEngine.new()
