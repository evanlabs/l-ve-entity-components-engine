local LogicComponent = LECEngine.Core.OOImplementation.newClassInheritsFrom(LECEngine.Core.Component)

LogicComponent._instancesCount = 0

function LogicComponent.new()
	local lc = LogicComponent:create()
	lc:init()
	return lc
end

function LogicComponent:init()
	LogicComponent:superClass().init(self)
	LogicComponent._instancesCount = LogicComponent._instancesCount + 1
	self._id = LogicComponent._instancesCount
end

function LogicComponent:getId()
	return self._id
end

function LogicComponent:update(p_dt)
	error ("Unimplemented update method for LogicComponent #" .. self:getId())
end

return LogicComponent
