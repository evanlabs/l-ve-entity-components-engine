local MixedComponent = LECEngine.Core.OOImplementation.newClassInheritsFrom(LECEngine.Core.Component)

MixedComponent._instancesCount = 0

function MixedComponent.new()
	local mc = MixedComponent:create()
	mc:init()
	return mc
end

function MixedComponent:init()
	MixedComponent:superClass().init(self)
	MixedComponent._instancesCount = MixedComponent._instancesCount + 1
	self._id = MixedComponent._instancesCount
	self._layer = 0
end

function MixedComponent:getId()
	return self._id
end

function MixedComponent:draw()
	error ("Unimplemented draw method for MixedComponent #" .. self:getId())
end

function MixedComponent:setLayer(p_layer)
	if p_layer < 0 then
		error ("Negative layers unsupported (trying to set MixedComponent #" .. self:getId() .. "'s layer to " .. p_layer .. ")")
	end
	if self:isAttached() then
		LECEngine.Core.GraphicSystem.get():changeLayer(self, p_layer)
	end
	self._layer = p_layer
end

function MixedComponent:getLayer()
	return self._layer
end

function MixedComponent:update(p_dt)
	error ("Unimplemented update method for MixedComponent #" .. self:getId())
end

return MixedComponent
