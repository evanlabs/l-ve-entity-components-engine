-- Entities (or GameObjects)

-- Every entity has a Transform (its position, rotation and scale in the World)

local Entity = LECEngine.Core.OOImplementation.newClass()
local Transform = LECEngine.Components.Transform

function Entity.new()
	local e = Entity:create()
	e:init()
	return e
end

function Entity:init()
	self._components = {}
	self:addComponent(Transform.new())
end

function Entity:addComponent(p_component)
	if self._components[p_component:getType()] ~= nil then
		error "You can't add two components of the same type onto an entity"
	end
	if p_component:getEntity() ~= nil then
		error ("The component #" .. p_component:getId() .. " already has a parent Entity")
	end
	if p_component._name ~= nil then
		self[p_component._name] = p_component
	end
	self._components[p_component:getType()] = p_component
	p_component:registerEntity(self)
	LECEngine.get():addComponent(p_component)
end

function Entity:removeComponent(p_componentType)
	if p_componentType == Transform then
		error "You cannot remove a Transform component"
	end
	self._components[p_componentType]:registerEntity(nil)
	LECEngine.get():removeComponent(self._components[p_componentType])
	self._components[p_componentType] = nil
end

function Entity:getComponent(p_componentType)
	return self._components[p_componentType]
end

function Entity:getComponents()
	return self._components
end

return Entity
