local GraphicSystem = LECEngine.Core.OOImplementation.newClass()

function GraphicSystem.new()
	local gs = GraphicSystem:create()
	GraphicSystem._instance = gs
	-- We pre-add some tables to optimize a bit (avoid some of LUA's rehashing)
	gs.componentsByLayer = {{}, {}, {}, {}}
	return gs
end

function GraphicSystem.get()
	return GraphicSystem._instance
end

function GraphicSystem:addComponent(p_component, p_layer)
	if not p_layer then
		p_layer = p_component:getLayer()
	end
	local layerTable = self.componentsByLayer[p_layer]
	if layerTable == nil then
		layerTable = {p_component}
		self.componentsByLayer[p_layer] = layerTable
	else
		layerTable[#layerTable + 1] = p_component
	end
	p_component._LECEngineGraphicSystem_componentTableIndex = #layerTable
end

function GraphicSystem:removeComponent(p_component)
	table.remove(self.componentsByLayer[p_component:getLayer()], p_component._LECEngineGraphicSystem_componentTableIndex)
end

function GraphicSystem:changeLayer(p_component, p_newLayer)
	if p_component:getLayer() ~= p_newLayer then
		self:removeComponent(p_component)
		self:addComponent(p_component, p_newLayer)
	end
end

function GraphicSystem:draw()
	for index, layerTable in pairs(self.componentsByLayer) do
		for _, component in pairs(layerTable) do
			component:draw()
		end
	end
end

return GraphicSystem
