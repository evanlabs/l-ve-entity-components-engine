--inspired by http://lua-users.org/wiki/ articles
local oo = {}
function oo.newClassInheritsFrom(p_baseClass)
	local newClassTable = {}
    local classMt = {__index = newClassTable}

    function newClassTable:create()
        local newInst = {}
        setmetatable(newInst, classMt)
        return newInst
    end

    if nil ~= p_baseClass then
        setmetatable(newClassTable, {__index = p_baseClass})
    end

    -- Return the class object of the instance
    function newClassTable:class()
        return newClassTable
    end

    -- Return the super class object of the instance
    function newClassTable:superClass()
        return p_baseClass
    end

    -- Return true if the caller is an instance of p_class
    function newClassTable:isInstanceOf(p_class)
        local result = false
        local currentClass = newClassTable

        while (nil ~= currentClass) and (false == result) do
            if currentClass == p_class then
                result = true
            else
                currentClass = currentClass:superClass()
            end
        end

        return result
    end

    return newClassTable
end

function oo.newClass()
	return oo.newClassInheritsFrom(nil)
end

return oo
