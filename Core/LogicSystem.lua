local LogicSystem = LECEngine.Core.OOImplementation.newClass()

function LogicSystem.new()
	local ls = LogicSystem:create()
	LogicSystem._instance = ls
	ls.components = {}
	return ls
end

function LogicSystem.get()
	return LogicSystem._instance
end

function LogicSystem:addComponent(p_component)
	self.components[p_component:getId()] = p_component
end

function LogicSystem:removeComponent(p_component)
	self.components[p_component:getId()] = nil
end

function LogicSystem:update(p_dt)
	for _, component in ipairs(self.components) do
		component:update(p_dt)
	end
end

return LogicSystem
